const canvas = document.getElementById("canvas");
const ctx = canvas.getContext("2d")

canvas.width = innerWidth;
canvas.height = innerHeight;

const canvasCol = canvas.width / 1000;
const canvasRow = canvas.height / 1000;

function create2DArray(num, row, col) {
    let array = [];
    for (let i = 0; i < row; i++){
        array.push([]);
        for (let j = 0; j < col; j++){
            array[i].push(num);
        }
    }
    return array;
}

const pathWidth = Math.round(canvas.width / (canvasRow * 85));
const pathHeight = Math.round(canvas.height / (canvasCol * 45));

const stageDivPathWidth = canvas.width / pathWidth;
const stageDivPathHeight = canvas.height / pathHeight;

function createMap() {
    let maxWalls = 20;
    let maxLength = 20;
    let map = create2DArray( 1, pathHeight, pathWidth);
    let currentRow = Math.floor(Math.random() * pathHeight);
    let currentCol = Math.floor(Math.random() * pathWidth)
    let directions = [[-1, 0], [1, 0], [0, -1], [0, 1]]
    let lastDirection = [];
    let randomDirection = null;

    while (maxWalls) {
    do {
        randomDirection = directions[Math.floor(Math.random() * directions.length)];
    } while ((randomDirection[0] === -lastDirection[0] &&
        randomDirection[1] === -lastDirection[1]) ||
        (randomDirection[0] === lastDirection[0] &&
        randomDirection[1] === lastDirection[1]));

        let randomLength = Math.ceil(Math.random() * maxLength);
        let wallLength = 0;
        while (wallLength < randomLength) {
            if (((currentRow === 0) && (randomDirection[0] === -1)) ||
            ((currentCol === 0) && (randomDirection[1] === -1)) ||
            ((currentRow === pathHeight - 1) && (randomDirection[0] === 1)) ||
            ((currentCol === pathWidth - 1) && (randomDirection[1] === 1)))
            {
                break;
            } else {
                map[currentRow][currentCol] = 0;
                currentRow += randomDirection[0];
                currentCol += randomDirection[1];
                wallLength++;
            }
        }
        if (wallLength) {
            lastDirection = randomDirection;
            maxWalls--
        }
    }
    return map;
}

function generateMapGraphics(map, val){
    let barrierStore = [];
    for (let i = 0; i < map.length; i++) {
        for (let j = 0; j < map[i].length; j++) {
            if (map[i][j] == val) {
                barrierStore.push({x:j, y:i})
            }
        }
    }
    return barrierStore;
}


function splitArrayMissingVal(array) {
    let splitArray = [];
    for (let i = 0; i < array.length; i++) {
        if (array[i] !== array[i+1] - 1) {
            if (array[i] && array[i + 1]){
                array.splice(i+1, 0, null)
            }
        }
    }
    let sequenceArray = null;
    for(let j = 0; j < array.length; j++){
        if(array[j] === null){
            sequenceArray = null;
        } else {
            if(!sequenceArray) {
                sequenceArray = [];
                splitArray.push(sequenceArray);
            }
            sequenceArray.push(array[j]);
        }
    }
    return splitArray;
}


function condensePath(pathArr) {
    let yGroup = [];
    for (let i = 0; i < pathHeight; i++) {
        let pathSect = pathArr.filter(path => path.y === i);
        yGroup.push(pathSect)
    }

    let xGroup = [];

    yGroup.forEach((group, yValue) => {
        let separateX = group.map(g => g.x);
        separateX = separateX.sort((a,b) => a - b);

        let splitX = splitArrayMissingVal(separateX)
        splitX.forEach(split => {
            let newCord = {x: split[0], length: split.length, y: yValue};
            xGroup.push(newCord)
        })
    })
    return xGroup;
}

let randomMap = createMap();
let barriers = generateMapGraphics(randomMap, 1);
barriers = condensePath(barriers);

function drawMap() {
    barriers.forEach(barrier => {
        ctx.fillStyle = "#000000";
        ctx.fillRect(barrier.x * stageDivPathWidth, barrier.y * stageDivPathHeight,
            stageDivPathWidth * barrier.length, stageDivPathHeight)
    })
}

(function animate() {
    requestAnimationFrame(animate);
    ctx.clearRect(0,0,canvas.width,canvas.height);

    drawMap();
})();